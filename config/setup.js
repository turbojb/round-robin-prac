// Load libraries models as global objects available to entire app
require("colors");
require("string").extendPrototype();

const fs = require("fs-extra");
const mongoose = require("mongoose");
const EventEmitter = require('events');
const WebApp = require(`${process.cwd()}/config/webapp`);
const dbOpts = {};


if(process.env.NODE_ENV && process.env.NODE_ENV === "production") {
	var env = "production";
	console.log(`${new Date()}: Starting App In Production :)`.cyan);
}
else {
	var env = "development";
	console.log(`${new Date()}: Starting App In Development :)`.cyan);
}		
		
global.envConfig = require("./env")[env];
global.myutils   = require("./util");

class RoundRobinApp extends EventEmitter {
	constructor() {
		super();

		var self = this;
		
		for(let file of fs.readdirSync("./app/models")) {
			let namearr = file.replace(/.js/g, "").split("_").map(name => {
				return name.charAt(0).toUpperCase() + name.slice(1);
			});

			let name = namearr.join("_");
			
			self[name] = require(`${process.cwd()}/app/models/${file}`);
		}

		mongoose.connect(envConfig.MONGO_URI, dbOpts, (err) => {
			if(err) {
				self.emit("error", err);
				return;
			}

			self.emit("ready");
		});
	}

	setupWebApp() {
		var self = this;
		
		self.webApp = new WebApp(self);

		self.webApp.on("ready", () => {
			self.emit("webAppReady");
		});
	}
}

module.exports = new RoundRobinApp();