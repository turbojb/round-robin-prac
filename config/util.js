module.exports = {
	randid: () => {
		var text = "";
		const size = 80;
  		const possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

  		for (var i = 0; i < size; i++) {
    		text += possible.charAt(Math.floor(Math.random() * possible.length));
  		}

  		return text;
	}
}