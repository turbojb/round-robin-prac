const async            = require("async");
const express          = require("express");
const EventEmiiter     = require("events");
const path             = require("path");
const passport         = require("passport");
const FacebookStrategy = require("passport-facebook")
const bodyParser       = require("body-parser");
const cookieParser     = require('cookie-parser');
const cookieEncrypter  = require('cookie-encrypter');

const HomeController  = require("../app/controllers/home_controller");
const RoomController  = require("../app/controllers/room_controller");

var expressLayouts    = require('express-ejs-layouts');
var logger            = require('morgan');

class WebApp extends EventEmiiter {
	constructor(base) {
		super();
		var self = this;
		
		self.base = base;
		self.app = express();
		self.app.set('views', `${process.cwd()}/app/views`);
		self.app.set('view engine', 'ejs');
		self.app.use(express.static(path.join(__dirname, '../public')));
		self.app.use(expressLayouts);
		self.app.use(logger('dev'));
		self.app.use(bodyParser.json()); 
		self.app.use(bodyParser.urlencoded({extended: true})); 
		self.app.use(cookieParser(envConfig.SECRET));
		self.app.use(cookieEncrypter(envConfig.SECRET));
		
		var homeController = new HomeController(self);
		//var RoomController = new RoomController(self);

		// catch 404 and forward to error handler
		self.app.use(function(req, res, next) {
		  var err = new Error('Not Found');
		  err.status = 404;
		  next(err);
		});

		self.app.listen(envConfig.PORT, () => {
			self.emit("ready");
		});
	}
}

module.exports = WebApp;