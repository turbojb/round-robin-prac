const roundRobinMusicApp = require(`${process.cwd()}/config/setup`);

roundRobinMusicApp.on("ready", () => {
	console.log("ALL MODELS ARE LOADED. Database is connected.".cyan);
	roundRobinMusicApp.setupWebApp();
});

roundRobinMusicApp.on("webAppReady", () => {
	console.log(`Webapp listening on PORT ${envConfig.PORT}`.cyan);
});

