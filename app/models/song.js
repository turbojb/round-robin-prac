const modelName = "Song";

var async       = require("async");
var crypto      = require("crypto");
var mongoose    = require("mongoose");
var Schema      = mongoose.Schema;

var fields = {
	title: {
		type: String,
		required: true
	},
	artist: { 
		type: String,
		required: true
	},
	file: {
		type: String,
		required: true
	},
	image: {
		type: String,
		required: true
	}
};

var ts = { 
	timestamps: {
		createdAt: "created_at",
		updatedAt: "updated_at"
	}
};

var schema = new Schema(fields, ts);

schema.index({'$**': 'text'});

schema.pre("save", next => {
	next();
});

module.exports = mongoose.model(modelName, schema);