const modelName = "RoomQueue";

var async       = require("async");
var crypto      = require("crypto");
var mongoose    = require("mongoose");
var Schema      = mongoose.Schema;

var fields = {
	current_user: {
		type: Schema.Types.ObjectId,
		ref: "User"
	},
	room: { 
		type: Schema.Types.ObjectId,
		ref: "Room"
	},
	owner: {
		type: Schema.Types.ObjectId,
		ref: "User",
		required: true
	},
	current_song: {
		type: Schema.Types.ObjectId,
		ref: "Song"
	},
	state: {
		type: String,
		enum: ["paused", "playing"],
		default: "paused"
	}
};

var ts = { 
	timestamps: {
		createdAt: "created_at",
		updatedAt: "updated_at"
	}
};

var schema = new Schema(fields, ts);

schema.pre("save", next => {
	next();
});

module.exports = mongoose.model(modelName, schema);