const modelName = "Room";

var async    = require("async");
var crypto   = require("crypto");
var mongoose = require("mongoose");
var Schema   = mongoose.Schema;

var schema = new Schema({
	lid: { 
		type: String, 
		index: true 
	},
	owner: { 
		type: Schema.Types.ObjectId, 
		ref: "User",
		required: true
	},
	invitees: [
		{
			type: Schema.Types.ObjectId,
			ref: "User"
		}
	],
	chat_queue: {
		type: Schema.Types.ObjectId,
		ref: "ChatQueue"
	},
	room_queue: {
		type: Schema.Types.ObjectId,
		ref: "RoomQueue"
	}
});


schema.statics.buildWithQueues = function(user, callback) {
	var self = this;
	var ChatQueue = mongoose.models.ChatQueue;
	var RoomQueue = mongoose.models.RoomQueue;

	if(!user) {
		return callback("Must Provide a User");
	}

	const opts = {
		owner: user.id
	};

	self.create(opts, (err, room) => {
		if(err) callback(err);

		const createChatQueue = function(cb) {
			const opts = { room: room.id };
			
			ChatQueue.create(opts, (err, chatQueue) => {
				if(err) throw err;
				room.chat_queue = chatQueue;
				room.save(cb);
			});
		}

		const createRoomQueue = function(cb) {
			const opts = { room: room.id, owner: user.id };
			
			RoomQueue.create(opts, (err, roomQueue) => {
				if(err) throw err;
				room.room_queue = roomQueue;
				room.save(cb);
			});
		}

		const funcs = [createChatQueue, createRoomQueue];

		async.parallel(funcs, (err) => {
	 		callback(null, room);
		});
	});
}

schema.pre("save", function(next) {
	var me = this;

	if(me.isNew) {
		crypto.randomBytes(128, (err, buffer) => {
			me.lid = buffer.toString("hex");	
			next();
		});
	}
	else {
		next();
	}
});

module.exports = mongoose.model(modelName, schema);
