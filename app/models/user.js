const modelName = "User";

var async       = require("async");
var crypto      = require("crypto");
var mongoose    = require("mongoose");
var validator   = require('validator');
var Schema      = mongoose.Schema;

var fields = {
	first_name: String,
	last_name: String,
	email: {
		type: String,
		required: true,
		unique: true,
		validate: {
			validator: (e, cb) => {
				const msg = "This is not a valid email";
				cb(validator.isEmail(e), msg);
			}
		}
	},
	lid: { 
		type: String, 
		index: true,
		unique: true
	},
	avatar: String,
	rooms: [ 
		{ 
			type: Schema.Types.ObjectId, 
			ref: "Room" 
		} 
	]
};

var ts = { 
	timestamps: {
		createdAt: "created_at",
		updatedAt: "updated_at"
	}
};

var schema = new Schema(fields, ts);

schema.pre("save", function(next) {
	var me = this; 

	if(me.isNew) {
		crypto.randomBytes(128, (err, buffer) => {
			me.lid = buffer.toString("hex");	
			next();
		});
	}
	else {
		next();
	}
});

module.exports = mongoose.model(modelName, schema);

