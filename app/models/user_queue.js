const modelName = "UserQueue";

var async       = require("async");
var crypto      = require("crypto");
var mongoose    = require("mongoose");
var Schema      = mongoose.Schema;

var fields = {
	user: {
		type: Schema.Types.ObjectId, 
		ref: "User" 
	},
	room_queue: { 
		type: Schema.Types.ObjectId, 
		ref: "RoomQueue" 
	},
	user_queue_assignments: [
		{
			type: Schema.Types.ObjectId,
			ref: "UserQueueAssignment"
		}
	]
};

var ts = { 
	timestamps: {
		createdAt: "created_at",
		updatedAt: "updated_at"
	}
};

var schema = new Schema(fields, ts);

schema.pre("save", next => {
	next();
});

module.exports = mongoose.model(modelName, schema);