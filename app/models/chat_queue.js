const modelName = "ChatQueue";

var async       = require("async");
var crypto      = require("crypto");
var mongoose    = require("mongoose");
var Schema      = mongoose.Schema;

var fields = {
	room: {
		type: Schema.Types.ObjectId,
		ref: "Room"
	},
	messages: [
		{ 
			type: Schema.Types.ObjectId,
			ref: "Message"
		}
	],
};

var ts = { 
	timestamps: {
		createdAt: "created_at",
		updatedAt: "updated_at"
	}
};

var schema = new Schema(fields, ts);

schema.pre("save", next => {
	next();
});

module.exports = mongoose.model(modelName, schema);