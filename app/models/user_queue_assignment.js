const modelName = "UserQueueAssignment";

var async       = require("async");
var crypto      = require("crypto");
var mongoose    = require("mongoose");
var Schema      = mongoose.Schema;

var fields = {
	user_queue: { 
		type: Schema.Types.ObjectId, 
		ref: "UserQueue" 
	},
	song: {
		type: Schema.Types.ObjectId,
		ref: "Song"
	}
};

var ts = { 
	timestamps: {
		createdAt: "created_at",
		updatedAt: "updated_at"
	}
};

var schema = new Schema(fields, ts);

schema.pre("save", next => {
	next();
});

module.exports = mongoose.model(modelName, schema);