class HomeController {
	constructor(webApp) {
		var self = this;
		var expressApp = webApp.app;
		var base = webApp.base;
		
		const viewFolder = "home";

		expressApp.get("/", (req, res, next) => {
			res.render(`${viewFolder}/index`);
		});
	}
}

module.exports = HomeController;

