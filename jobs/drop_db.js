var app = require(`${process.cwd()}/config/setup`);
const async    = require("async");
const readline = require('readline');
const mongoose = require("mongoose");

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

//TODO add check to not do this in production mode

rl.question('Drop The Database? ARE YOU SURE?\n\n'.yellow, (answer) => {
  // TODO: Log the answer in a database
  
  if(answer.toLowerCase() == "yes") {
  	const collections = mongoose.connection.collections;

  	const dropCol = function(col, cb) {
  		try {
  			col.drop((err) => {
  				//if(err) throw err;
  				cb();
  			});
  		}
  		catch(err) {
  			cb();
  		}
  	}

  	async.each(collections, dropCol, () => {
  		console.log("done".cyan);
  		mongoose.connection.close();
  	});
  }
  else {
  	console.log("Abort".red);
  	mongoose.connection.close();
  }

  rl.close();
});