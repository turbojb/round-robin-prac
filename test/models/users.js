var app = require(`${process.cwd()}/config/setup`);

const assert = require('assert');
const faker  = require("faker");
const expect = require('chai').expect
const mocha  = require("mocha");

console.log(`${new Date()}: Starting Models Test`.cyan);

describe('Models', () => {
  describe('User', () => {
  	it("Create New User Should Be Valid", () => {
  		const opts = {
  			first_name: faker.name.firstName(),
  			last_name: faker.name.lastName(),
  			email: faker.internet.email()
  		};
  		
  		var user = new app.User(opts);
  		var err = user.validateSync();
		assert(true, typeof err === typeof undefined);
  	});

  	it("Create New user without email should fail", () => {
  		const opts = {
	  		first_name: faker.name.firstName(),
	  		last_name: faker.name.lastName()
	  	};
	  		
	  	var user = new app.User(opts);
	  	var err = user.validateSync();
	  	
	  	assert(true, typeof err === typeof object);
  	});

  	it("Create New user with bogus email should fail", (done) => {
  		const opts = {
  			first_name: faker.name.firstName(),
  			last_name: faker.name.lastName(),
  			email: "blah"
  		}

  		var user = app.User(opts);
  		
  		user.validate((err) => {
  			assert.equal("This is not a valid email", err.errors["email"].message);
  		  done();
      });
  	});
  });
});

