var app = require(`${process.cwd()}/config/setup`);

const assert = require('assert');
const faker  = require("faker");
const expect = require('chai').expect
const mocha  = require("mocha");

console.log(`${new Date()}: Starting All Test`.cyan);

var opts = {
	first_name: faker.name.firstName(),
	last_name: faker.name.lastName(),
	email: faker.internet.email()
};

var user = new app.User(opts);
var room;
var roomQueue;
var errs = user.validateSync();

if(typeof errs === typeof undefined) {

	console.log("Create User".cyan);

	user.save((err) => {	
		if(err) {
			console.log("Error".red);
			console.log(err);
		}
		console.log("Create Room".cyan);

		
		app.Room.buildWithQueues(user, (err, room) => {
			var assoc = [
				"chat_queue", 
				{ 
					path: "room_queue",
					populate: {
						path: "current_song"
					}
				}, 
				"invitees", 
				"owner"
			];
			
			var query = app.Room.findById(room.id).populate(assoc);

			query.exec((err, room) => {
				console.log(room);
			});
		});

	});
}
